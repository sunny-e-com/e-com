from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('cart/', views.cart, name='cart'),
    path('update_cart/<int:pid>', views.update_cart, name='update_cart'),
    path('delete_cart/<int:pid>', views.delete_cart, name='delete_cart'),
    path('admin_cart/', views.admin_cart, name='admin_cart'),
    path('checkout/', views.checkout, name='checkout'),
    path('success/', views.success, name='success'),
    path('admin_checkout/', views.admin_checkout, name='admin_checkout'),
]

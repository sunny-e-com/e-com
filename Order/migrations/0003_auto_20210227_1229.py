# Generated by Django 3.1.7 on 2021-02-27 06:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Order', '0002_cart_quantity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='quantity',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

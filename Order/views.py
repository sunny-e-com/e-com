from django.shortcuts import render, redirect
from .models import Cart, Checkout
import razorpay
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

def cart(request):
    data = Cart.objects.filter(user=request.user.id)
    user_id = str(request.user.id)
    cart = Cart.objects.raw('SELECT id,total(price) as total FROM Order_cart WHERE user_id="' + user_id + '"')
    return render(request, 'user_temp/cart.html', {'data': data, 'cart': cart})


def admin_cart(request):
    cart = Cart.objects.all()
    return render(request, 'admin_temp/cart.html', {'cart': cart})


def update_cart(request, pid):
    try:
        if request.method == "POST":
            pro_id = request.POST.get('pro_id')
            quantity = request.POST.get('quantity')
            price = request.POST.get('price')
            user = str(request.user.id)
            cart = Cart.objects.raw(
                'UPDATE Order_cart SET price="' + price + '" * "' + quantity + '" , quantity="' + quantity + '" WHERE product_id="' + pro_id + '" AND user_id="' + user + '"')
            return render(request, 'user_temp/update_cart.html', {'cart': cart})
        else:
            data = Cart.objects.get(id=pid)
            return render(request, 'user_temp/update_cart.html', {'data': data})
    except TypeError:
        data = Cart.objects.filter(user=request.user)
        return redirect('cart')


def delete_cart(request, pid):
    data = Cart.objects.get(id=pid)
    data.delete()
    return redirect('cart')


def checkout(request):
    data = Cart.objects.filter(user=request.user.id)
    user_id = str(request.user.id)
    cart = Cart.objects.raw('SELECT id,total(price) as total FROM Order_cart WHERE user_id="' + user_id + '"')
    if request.method == 'POST':
        ammount = float(request.POST.get("ammount")) * 100
        product = request.POST.get("product")
        client = razorpay.Client(auth=("rzp_test_2gLzdXefPIBGFT", "IDLkgnGKP9gHqbxAuQFeKwHq"))
        payment = client.order.create({'amount': ammount, 'currency': 'INR', 'payment_capture': '1'})
        coffee = Checkout(name=request.user, product=product, ammount=ammount, payment_id=payment['id'])
        coffee.save()
        return render(request, 'user_temp/checkout.html', {'payment': payment})
    return render(request, 'user_temp/checkout.html', {'data': data, 'cart': cart})


@csrf_exempt
def success(request):
    if request.method == 'POST':
        a = request.POST
        order_id = ""
        for key, val in a.items():
            if key == 'razorpay_order_id':
                order_id = val
                break
        user = Checkout.objects.filter(payment_id=order_id).first()
        user.paid = True
        user.save()
        try:
            if user.paid == True:
                user = str(request.user.id)
                cart = Cart.objects.raw(
                    'DELETE FROM Order_cart WHERE user_id="' + user + '"')
                return render(request, 'user_temp/success.html', {'cart': cart})
        except TypeError:
            return render(request, 'user_temp/success.html')


def admin_checkout(request):
    data = Checkout.objects.all()
    return render(request, 'admin_temp/checkout.html', {'data': data})

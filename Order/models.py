from django.db import models
from Account.models import User
from Product.models import Products


# Create your models here.

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user", null=True,
                             blank=True)
    product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name="product", null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True)
    price = models.PositiveIntegerField(null=True)
    status = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.price)


class Checkout(models.Model):
    name = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_name", null=True,
                             blank=True)
    product = models.CharField(max_length=40,null=True)
    ammount = models.CharField(max_length=40)
    payment_id = models.CharField(max_length=40)
    paid = models.BooleanField(default=False)

from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect

from Order.models import Cart
from .models import Products, Category, SubCategory
from .form import form_insert_category, form_insert_subcategory, PersonForm, form_insert_product
from django.contrib.auth.decorators import login_required


# Create your views here.

def index(request):
    if request.user.is_superuser:
        return render(request, 'admin_temp/index.html')
    else:
        data = Products.objects.all()
        user = str(request.user.id)
        return render(request, 'user_temp/index.html', {'data': data})


def product(request, pid):
    user = request.user.id
    data = Products.objects.get(id=pid)
    pro = Cart.objects.filter(user=user, product=data)
    if pro:
        return redirect('cart')
    else:
        if request.method == 'POST':
            quantity = request.POST.get('quantity')
            product_price = request.POST.get('product_price')
            price_log = int(quantity) * int(product_price)
            car = Cart.objects.create(user=request.user, product=data, quantity=quantity, price=price_log)
            car.status = True
            car.save()
            return redirect('cart')
        else:
            data = Products.objects.get(id=pid)
    return render(request, 'user_temp/product.html', {'data': data})


def load_branches(request):
    country_id = request.GET.get('category')
    cities = SubCategory.objects.filter(category_id=country_id).order_by('name')
    return render(request, 'admin_temp/designation_dropdown_list_options.html', {'cities': cities})


def insert_product(request):
    data = Products.objects.all()
    if request.method == 'POST':
        form = PersonForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('product_view')
    else:
        form = PersonForm()
    return render(request, 'admin_temp/insert_product.html', {'form': form, 'data': data})


def insert_category(request):
    data = Category.objects.all()
    if request.method == 'POST':
        form = form_insert_category(request.POST)
        if form.is_valid():
            form.save()
            return redirect('insert_category')
    else:
        form = form_insert_category()
    return render(request, 'admin_temp/insert_category.html', {'form': form, 'data': data})


def update_category(request, pid):
    new = Category.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_category(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('insert_category')
    else:
        form = form_insert_category(instance=new)
    return render(request, 'admin_temp/edit.html', {'form': form})


def insert_subcategory(request):
    data = SubCategory.objects.all()
    if request.method == 'POST':
        form = form_insert_subcategory(request.POST)
        if form.is_valid():
            form.save()
            return redirect('insert_subcategory')
    else:
        form = form_insert_subcategory()
    return render(request, 'admin_temp/insert_subcategory.html', {'form': form, 'data': data})


def update_subcategory(request, pid):
    new = SubCategory.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_subcategory(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('insert_subcategory')
    else:
        form = form_insert_subcategory(instance=new)
    return render(request, 'admin_temp/edit.html', {'form': form})


def product_view(request):
    data = Products.objects.all()
    return render(request, 'admin_temp/product_view.html', {'data': data})


def update_product(request, pid):
    new = Products.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_product(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('product_view')
    else:
        form = form_insert_product(instance=new)
    return render(request, 'admin_temp/edit.html', {'form': form})

from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('ajax/load-branches/', views.load_branches, name='ajax_load_branches'),
    path('insert_product/', views.insert_product, name='insert_product'),
    path('insert_category/', views.insert_category, name='insert_category'),
    path('insert_subcategory/', views.insert_subcategory, name='insert_subcategory'),
    path('product_view/', views.product_view, name='product_view'),
    path('product/<int:pid>', views.product, name='product'),
    path('update_product/<int:pid>', views.update_product, name='update_product'),
    path('update_category/<int:pid>', views.update_category, name='update_category'),
    path('update_subcategory/<int:pid>', views.update_subcategory, name='update_subcategory'),
]

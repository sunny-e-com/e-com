# Generated by Django 3.1.7 on 2021-02-26 06:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0004_auto_20210226_1225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='pro_del_price',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Product Deleted Price'),
        ),
    ]

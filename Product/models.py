from django.db import models

CHOICES_PRO_SIZE = (
    ('small', 'small'),
    ('medium', 'medium'),
    ('large', 'large'),
    ('X-large', 'X-large'),
)


class Category(models.Model):
    name = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="category", verbose_name="category",
                                 null=True)
    name = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.name


# Create your models here.
class Products(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="pro_cat", verbose_name="category",
                                 null=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE, related_name="pro_sub_cat",
                                    verbose_name="subcategory", null=True)
    pro_sku = models.PositiveIntegerField(verbose_name="SKU", null=True, blank=True)
    occasion = models.CharField(max_length=20, verbose_name="Occasion", null=False, blank=True)
    work = models.CharField(max_length=20, verbose_name="Work", null=False, blank=True)
    stitching_type = models.CharField(max_length=20, verbose_name="Stitching Type", null=False, blank=True)
    length = models.CharField(max_length=20, null=False, blank=True)
    fabric = models.CharField(max_length=20, null=False, blank=True)
    color = models.CharField(max_length=20, null=False, blank=True)
    top_fabric = models.CharField(max_length=20, verbose_name="Top Fabric", null=False, blank=True)
    top_length = models.CharField(max_length=20, verbose_name="Top Length", null=False, blank=True)
    dupatta_fabric = models.CharField(max_length=20, verbose_name="Dupatta Fabric", null=False, blank=True)
    dupatta_length = models.CharField(max_length=20, verbose_name="Dupatta Length", null=False, blank=True)
    bottom_length = models.CharField(max_length=20, verbose_name="Bottom Length", null=False, blank=True)
    url = models.URLField(max_length=40, verbose_name="Url", null=False, blank=True)
    saree_fabric = models.CharField(max_length=20, verbose_name="Saree Fabric", null=False, blank=True)
    saree_color = models.CharField(max_length=20, verbose_name="Saree Colour", null=False, blank=True)
    saree_length = models.CharField(max_length=20, verbose_name="Saree Length", null=False, blank=True)
    blouse_color = models.CharField(max_length=20, verbose_name="Blouse Colour", null=False, blank=True)
    blouse_fabric = models.CharField(max_length=20, verbose_name="Blouse Fabric", null=False, blank=True)
    pro_img = models.ImageField(upload_to='files/', height_field="image_height",
                                width_field="image_width", verbose_name="Product Image", blank=True, null=True)
    pro_name = models.CharField(max_length=30, verbose_name="Product Name", null=True, blank=True)
    pro_short_dec = models.TextField(verbose_name="Product Short Desc.", null=True)
    pro_long_dec = models.TextField(verbose_name="Product Long Desc.", null=False, blank=True)
    pro_price = models.PositiveIntegerField(verbose_name="Product Price", null=True)
    pro_del_price = models.PositiveIntegerField(verbose_name="Product Deleted Price", null=True, blank=True)
    pro_offer_price = models.PositiveIntegerField(verbose_name="Product Offer Price", null=True, blank=True)
    pro_size = models.CharField(max_length=10, choices=CHOICES_PRO_SIZE, default="large",
                                verbose_name="Product Offer Price", null=True)
    image_height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="60")
    image_width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="60")

    def __str__(self):
        return self.pro_name

from django import forms
from .models import Products, Category, SubCategory


class DateInput(forms.DateInput):
    input_type = 'date'
    format = '%d/%m/%Y'


class form_insert_product(forms.ModelForm):
    class Meta:
        model = Products
        fields = 'category', 'subcategory', 'pro_img', 'pro_name', 'pro_short_dec', 'pro_long_dec', 'pro_price', 'pro_offer_price', 'pro_size',
        widgets = {
            'pro_short_dec': forms.Textarea(attrs={'rows': 4, 'cols': 40}),
            'pro_long_dec': forms.Textarea(attrs={'rows': 4, 'cols': 40}),
        }


class PersonForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = (
            'category', 'subcategory', 'pro_name', 'pro_img', 'pro_price','pro_del_price', 'pro_offer_price', 'pro_size', 'pro_sku',
            'occasion', 'work', 'stitching_type', 'length', 'fabric', 'color', 'top_fabric', 'top_length',
            'dupatta_fabric',
            'dupatta_length', 'bottom_length', 'url', 'saree_fabric', 'saree_color', 'saree_length', 'blouse_color',
            'blouse_fabric', 'pro_long_dec', 'pro_short_dec')
        widgets = {
            'pro_short_dec': forms.Textarea(attrs={'rows': 4, 'cols': 40}),
            'pro_long_dec': forms.Textarea(attrs={'rows': 4, 'cols': 40}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['subcategory'].queryset = SubCategory.objects.none()

        if 'category' in self.data:
            try:
                category_id = int(self.data.get('category'))
                self.fields['subcategory'].queryset = SubCategory.objects.filter(category_id=category_id).order_by(
                    'name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['subcategory'].queryset = self.instance.category.subcategory_set.order_by('name')


class form_insert_category(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class form_insert_subcategory(forms.ModelForm):
    class Meta:
        model = SubCategory
        fields = '__all__'

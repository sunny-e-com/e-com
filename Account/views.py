from django.contrib.auth.views import LoginView
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from Order.models import Cart, Checkout
from .form import SignUpForm, form_contact_us, form_contact_to_user, update_signup
from .models import ContactUs, User


# Create your views here.

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'account/register.html', {'form': form})


def user_detail(request):
    data = User.objects.all()
    return render(request, 'admin_temp/user_detail.html', {'data': data})


def contact(request):
    user = request.user
    if request.method == 'POST':
        form = form_contact_us(request.POST)
        if form.is_valid():
            data = form.save()
            data.email = user
            data.save()
            return redirect('contact')
    else:
        form = form_contact_us()
    return render(request, 'user_temp/contact.html', {'form': form})


def contact_to_admin(request):
    data = ContactUs.objects.raw('SELECT id,email_id,message_to_admin,message_to_user FROM Account_contactus ORDER BY '
                                 'id DESC')
    return render(request, 'admin_temp/contact.html', {'data': data})


def contact_to_user(request, pid):
    new = ContactUs.objects.get(id=pid)
    if request.method == 'POST':
        form = form_contact_to_user(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('contact_to_admin')
    else:
        form = form_contact_to_user(instance=new)
    return render(request, 'admin_temp/edit.html', {'form': form})


def account(request):
    contact = ContactUs.objects.filter(email=request.user.id)
    cart = Cart.objects.filter(user=request.user.id)
    checkout = Checkout.objects.filter(name=request.user.id)
    return render(request, 'user_temp/account.html', {'contact': contact, 'cart': cart, 'checkout': checkout})


def update_registration(request, pid):
    new = User.objects.get(id=pid)
    if request.method == 'POST':
        form = update_signup(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = update_signup(instance=new)
    return render(request, 'user_temp/edit.html', {'form': form})

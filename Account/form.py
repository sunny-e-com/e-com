from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User
from .models import ContactUs


class DateInput(forms.DateInput):
    input_type = 'date'
    format = '%d/%m/%Y'


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'Address')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Email already Exist')
        return email


class update_signup(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'Address',)


class form_contact_us(forms.ModelForm):
    class Meta:
        model = ContactUs
        fields = ('message_to_admin',)


class form_contact_to_user(forms.ModelForm):
    class Meta:
        model = ContactUs
        fields = ('message_to_user',)

from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    Address = models.TextField(null=True)


# Create your models here.
class ContactUs(models.Model):
    email = models.ForeignKey(User, on_delete=models.CASCADE, related_name="User_form", null=True,
                              blank=True, )
    message_to_admin = models.TextField(verbose_name="Message")
    message_to_user = models.TextField(verbose_name="Message", null=False, blank=True)

    def __str__(self):
        return self.message_to_admin

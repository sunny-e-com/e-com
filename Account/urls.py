from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth import views as djangoview


urlpatterns = [
    path('register/', views.register, name='register'),
    path('update_registration/<int:pid>', views.update_registration, name='update_registration'),
    path('user_detail/', views.user_detail, name='user_detail'),
    path('contact/', views.contact, name='contact'),
    path('contact_to_admin/', views.contact_to_admin, name='contact_to_admin'),
    path('account/', views.account, name='account'),
    path('contact_to_user/<int:pid>', views.contact_to_user, name='contact_to_user'),
    path('login/', djangoview.LoginView.as_view(template_name='account/login.html'), name='login'),
    path('logout/', djangoview.LogoutView.as_view(), name='logout'),
    # reset password
    path('password-change/', djangoview.PasswordChangeView.as_view(template_name='account/change_password.html'),
         name='password_change'),
    path('password_change/done/',
         djangoview.PasswordChangeDoneView.as_view(template_name='account/password_changed_done.html'),
         name='password_change_done'),

    # forgot password
    path('password-reset/',
         djangoview.PasswordResetView.as_view(template_name='account/password_reset.html',
                                              subject_template_name='account/password_reset_subject.txt',
                                              email_template_name='account/password_reset_email.html'),
         name='password_reset'),
    path('password-reset/done/',
         djangoview.PasswordResetDoneView.as_view(template_name='account/password_reset_done.html'),
         name='password_reset_done', ),
    path('password-reset-confirm/<uidb64>/<token>/',
         djangoview.PasswordResetConfirmView.as_view(template_name='account/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         djangoview.PasswordResetCompleteView.as_view(template_name='account/password_reset_complete.html'),
         name='password_reset_complete'),
]
